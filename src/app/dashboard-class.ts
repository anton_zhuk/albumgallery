export class DashBoard{
    id: number;
    title: string;
    data: {
        author: string;
        genre: string;
        year: number;
        songs: Array<string>;
    }
    path: string;
}