import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { DashBoard } from '../dashboard-class';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ HttpService ]
})
export class DashBoardComponent implements OnInit {
  
  dash: DashBoard[] = [];

  constructor(private httpService: HttpService, private router: Router) {}

  ngOnInit() {
    this.httpService.getData().subscribe((data:DashBoard) => this.dash = data["AlbumList"]);
  }
}

