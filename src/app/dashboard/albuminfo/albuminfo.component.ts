import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpService } from '..//../http.service';
import { DashBoard } from '..//../dashboard-class';
import { Router } from '@angular/router';


@Component({
  selector: 'app-albuminfo',
  templateUrl: './albuminfo.component.html',
  styleUrls: ['./albuminfo.component.css'],
  providers: [ HttpService ]
})
export class AlbumInfoComponent {
  
  private idAlbum: number;
  private subscription: Subscription;
  constructor(private activateRoute: ActivatedRoute, private httpService: HttpService, private router: Router) {       
    this.subscription = activateRoute.params.subscribe(params => this.idAlbum=params['id']);
  }
  
  dash: DashBoard[] = [];
  ngOnInit() {
    this.httpService.getData().subscribe((data:DashBoard) => this.dash=data["AlbumList"]);
  }

}
