import { Component } from '@angular/core';
  
@Component({
    selector: 'not-found-app',
    template: `
                <div class="jumbotron jumbotron-fluid ">
                    <div class="container">
                        <h1 class="display-4">ERROR</h1>
                        <p class="lead">Requested page does not exist</p>
                    </div>
                </div>
            `
})
export class NotFoundComponent { }